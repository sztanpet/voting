require('utility')
local sess = require('session')

return function()
  sendHeaders()
  local template = view('index.tpl')
  local vars     = {}
  local session  = sess.get()
  
  if session and session.user then
    vars.member = session.user
  end
  
  ngx.print(template(vars))
end
