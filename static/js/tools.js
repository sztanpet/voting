function in_array( needle, haystack ) {
  
  for ( var i in haystack )
    if ( haystack[ i ] == needle )
      return true;
  
  return false;
  
}

var $j = jQuery.noConflict();
$j(document).ready(function() {
  
  $j('#systemmessageclose a').click( function() {
    
    $j('#systemmessage').slideUp(150);
    return false;
    
  });
  
  runIfExists('.clearonclick', setupClearOnClick );
  runIfExists('.confirm', setupConfirm );
  runIfExists('#votingpage', setupPollVote );
  
  $j('#scriptingcontainer').show();
  
});

function runIfExists( selector, func ) {
  
  var elem = $j( selector );
  if ( elem.length > 0 )
    func( elem );
  
}

function setupClearOnClick( elem ) {
  
  elem.on('focusin', function() {
    
    if ( $j(this).val() == $j(this).attr('data-origval') )
      $j(this).val('');
    
  }).on('focusout', function() {
    
    if ( !$j(this).val() )
      $j(this).val( $j(this).attr('data-origval') );
    
  });
  
}

function setupConfirm( elem ) {
  
  elem.click( function( e ) {
    return confirm( $j(this).attr('data-confirm') );
  });
  
}

function setupPollVote() {
  
  var poll = new VotePoll( pollurl ); // pollurl from global scope
  
  var key    = 'vote-' + $j('.answerlist').attr('data-questionid');
  var answer = store.get( key );
  if ( !answer ) {
    answer = $j.cookie( key );
  }
  
  if ( answer ) {
    $j.cookie( key, answer, {expires: 365});
    store.set( key, answer );
  }
  
  $j('.answerlist .answer').on('click', function(e) {
    var self = this;
    e.preventDefault();
    if ( !answer ) {
      
      $j.ajax({
        cache   : false,
        dataType: 'json',
        type    : 'GET',
        url     : $j(this).attr('href'),
        success : function(data) {
          
          if ( typeof "data" != "object" )
            return;
          
          if ( data.success ) {
            answer = $j(self).parents('li').attr('data-answerid');
            $j.cookie( key, answer, {expires: 365});
            store.set( key, answer );
            updateCounts(data);
          } else {
            alert('You have already voted!');
          }
          
        }
      });
      
    } else {
      alert('You have already voted!');
    }
    
  });
  
}

function VotePoll( url ) {
  
  this.onSuccess   = $j.proxy( this.success, this );
  this.onPoll      = $j.proxy( this.poll, this );
  this.lastrequest = null;
  this.ratelimit   = 0;
  
  this.ajaxoptions = {
    cache   : false,
    success : this.onSuccess,
    complete: this.onPoll,
    dataType: 'json',
    type    : 'GET',
    url     : url
  };
  
  setTimeout( this.onPoll, 1000 ); // first poll always a second out
  
}
VotePoll.prototype.poll = function() {
  
  var now = (new Date).getTime();
  if ( !this.lastrequest )
    this.lastrequest = now;
  
  if ( (now - this.lastrequest) < this.ratelimit ) {
    
    setTimeout( this.onPoll, this.ratelimit );
    return;
    
  }
  
  this.lastrequest = now;
  $j.ajax( this.ajaxoptions );
  
};
VotePoll.prototype.success = function( data ) {
  
  updateCounts(data);
  
};

function updateCounts( data ) {
  //{"success":true,"data":{"3":"1","6":"0"}}
  
  if ( !data.success )
    return;
  
  var totalcount = 0;
  var itemcount  = 0;
  $j.each(data.data, function() { itemcount++; totalcount += parseInt(this, 10); });
  
  if ( $j('.answerlist .answeritem').length != itemcount ) {
    
    // something changed, maybe the answers, refresh the page as it's no longer valid
    document.location.href = document.location.href;
    return;
    
  }
  
  if ( $j('.answerlist').attr('data-totalcount') == totalcount ) // nothing changed
    return;
  
  $j('.answerlist').attr('data-totalcount', totalcount);
  $j('.title .count').text(totalcount);
  
  $j.each(data.data, function(id, count) {
    var htmlid  = '#answer-' + id;
    var percent = count / totalcount * 100;
    
    $j(htmlid + ' .answerpercent').css('width', percent + '%');
    $j(htmlid + ' .count').text(count);
  });
  
}
