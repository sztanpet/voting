require "utility"
module(..., package.seeall)
local context, tconcat, tinsert = ngx.ctx, table.concat, table.insert

function getRedisConnection()
  if not context.redis then
    local redis = (require "resty.redis"):new()
    redis:set_timeout(1000)
    redis:set_keepalive(0, 1000)
    if not redis:connect("127.0.0.1", 6379) then
      ngx.log(ngx.ERR, "failed to connect to redis")
    end
    
    context.redis = redis
  end
  
  return context.redis
end

function query(sql)
  if type(sql) == 'table' then sql = tconcat(sql) end
  
  local params = {
    method = ngx.HTTP_POST,
    body   = sql
  }

  local result = ngx.location.capture("/postgres", params )
  if result.status ~= ngx.HTTP_OK or not result.body then
    return nil
  end
  
  if not json then json = require "cjson" end
  return (json.decode(result.body) or {})
end

function escapePostgresParam(...)
  local url      = '/postgresescape?param='
  local requests = {}
  
  for i = 1, select('#', ...) do
    local param = ngx.escape_uri((select(i, ...)))
    
    tinsert(requests, {url .. param})
  end
  
  local results = {ngx.location.capture_multi(requests)}
  for k, v in pairs(results) do
    results[k] = trim(v.body)
  end
  
  return unpack(results)
end

return _M
