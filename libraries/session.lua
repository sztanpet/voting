local db = require "db"
module(..., package.seeall)

local sessionexpireseconds = 7600
function getRedisKey(sessionid)
  if not sessionid then return end
  
  return 'votingsession:' .. sessionid
end

function getID()
  local sessionid = ngx.var.cookie_SESSIONID
  
  if not sessionid or #sessionid ~= 32 or (sessionid:find('[^a-zA-Z0-9]')) then return end
  return sessionid
end

function get()
  local sessionid  = getID()
  if not sessionid then return end
  local redis      = db.getRedisConnection()
  local session    = redis:get(getRedisKey(sessionid))
  
  if session == ngx.null or not session or #session < 2 then return end
  
  if not json then json = require "cjson" end
  session = json.decode(session)
  
  return session, redis
end

function destroy(sessionid, redis)
  ngx.header['Set-Cookie'] = 'SESSIONID=0; path=/;'
  
  local sessionid  = sessionid or getID()
  if not sessionid then return end
  local sessionkey = getRedisKey(sessionid)
  local redis      = redis or db.getRedisConnection()
  
  return redis:del(sessionkey), redis
end

function start(data)
  local result = ngx.location.capture('/rand32')
  
  if result.status ~= ngx.HTTP_OK or not result.body then
    ngx.log(ngx.ERR, 'FAILED TO GET /rand32')
  end
  
  local sessionid  = trim(result.body)
  result           = nil
  
  ngx.header['Set-Cookie'] = 'SESSIONID=' .. sessionid .. '; path=/;'
  local result, redis = sync(sessionid, data)
  
  return sessionid, result, redis
end

function sync(sessionid, data, redis)
  local sessionkey = getRedisKey(sessionid or getID())
  
  if not sessionkey then return end
  if not redis then redis = db.getRedisConnection() end
  
  if not data then
    data = '[]'
  else
    if not json then json = require "cjson" end
    data = json.encode(data)
  end
  
  return redis:setex(sessionkey, sessionexpireseconds, data), redis
end

return _M;
