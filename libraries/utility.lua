
function nl2br(s)
  if not s then return '' end
  
  return (s:gsub('\n', '\n<br/>'))
end

function sendHeaders()
  local headers = ngx.header
  
  headers.content_type    = 'text/html; charset=utf-8'
  headers.x_ua_compatible = 'chrome=1'
end

function redirect(to)
  ngx.header.location = to
  ngx.exit(ngx.HTTP_MOVED_TEMPORARILY)
end

function getTemplate(template)
  local result = ngx.location.capture("/templates/" .. template )
  
  if result.status ~= ngx.HTTP_OK or not result.body then
    return nil, result
  end
  
  return result.body
end

-- Simplistic HTML escaping.
function escape(s)
  if not s then return '' end
  s = tostring(s)
  return (s:gsub('&', '&amp;'):gsub('<', '&lt;'):gsub('>', '&gt;'):gsub([["]], '&quot;'):gsub([[']], '&apos;'))
end

function trim(s)
  if not s then return '' end
  
  return (s:gsub('^%s*(.-)%s*$', '%1'))
end

-- Used in template parsing to figure out what each {} does.
local VIEW_ACTIONS = {
  ['{%'] = function(code)
    return code
  end,

  ['{{'] = function(code)
    return ('_result[#_result+1] = %s'):format(code)
  end,

  ['{('] = function(code)
    return ([[ 
      if not _children[%s] then
        _children[%s] = view(%s)
      end

      _result[#_result+1] = _children[%s](getfenv())
    ]]):format(code, code, code, code)
  end,

  ['{<'] = function(code)
    return ('_result[#_result+1] =  escape(%s)'):format(code)
  end,
}

-- Takes a view template and optional name (usually a file) and 
-- returns a function you can call with a table to render the view.
function compile_view(tmpl, name)
  local tmpl = tmpl .. '{}'
  local code = {'local _result, _children = {}, {}\n'}

  for text, block in string.gmatch(tmpl, "([^{]-)(%b{})") do
    local act = VIEW_ACTIONS[ block:sub(1, 2) ]
    local output = text

    if act then
      code[#code+1] =  '_result[#_result+1] = [[' .. text .. ']]'
      code[#code+1] = act( block:sub(3, -3) )
    elseif #block > 2 then
      code[#code+1] = '_result[#_result+1] = [[' .. text .. block .. ']]'
    else
      code[#code+1] =  '_result[#_result+1] = [[' .. text .. ']]'
    end
  end

  code[#code+1] = 'return table.concat(_result)'

  code = table.concat(code, '\n')
  local func, err = loadstring(code, name)

  if err then
    assert(func, err)
  end

  return function(context)
    assert(context, "You must always pass in a table for context.")
    setmetatable(context, {__index=_G})
    setfenv(func, context)
    return func()
  end
end

-- Crafts a new view from the given file in the TEMPLATES directory.
-- If the ENV[PROD] is set to something then it will do this once.
-- Otherwise it returns a function that reloads the template since you're
-- in developer mode.
function view(name)
  local tempf = getTemplate(name)

  if PRODUCTION then
    return compile_view(tempf, name)
  else
    return function (params)
      assert(tempf, "failed gettig template: " .. name )
      return compile_view(tempf, name)(params)
    end
  end
end

--[[
local function idTransform()
  local index = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  local tmp   = {}
  local hash  = ngx.md5('just a hash') .. ngx.md5('another one to make it 64 char long')
  
  for i = 1, #index do
    table.insert(tmp, hash:sub(i, i))
  end
  return table.concat(tmp)
end

do
  local index = idTransform()
  local base  = #index
  
  function idEncrypt( id )
    local out = {}
    id        = id + math.pow( base, 2 ) -- +padding
    
    do
      local t = math.floor(math.log(id) / math.log(base))
      while true do
        local pow = math.pow(base, t)
        local ix  = math.floor(id / pow) % base
        table.insert(out, index:sub(ix + 1, ix + 1))
        t = t - 1
        if t < 0 then break end
      end
    end
    
    out = table.concat(out)
    return out:reverse()
  end

  function idDecrypt(id)
    local id  = string.reverse(id)
    local len = #id - 1
    local out = 0
    
    for i = 0, len do
      local pow = math.pow(base, len - i)
      out = out + (index:find(id:sub(i + 1, i + 1) ) - 1 ) * pow
    end
    
    out = out - math.pow( base, 2 ); -- -padding
    out = ('%f'):format(out);
    return tonumber(out:sub(1, (out:find('.'))))
  end
end

function numberFormat(number, decimals, decimalpoint, thousandsep)
  local decimals     = decimals or 2
  local decimalpoint = decimalpoint or '.'
  local thousandsep  = thousandsep or ' '
  local out          = {}
  
  if number < 0 then
    table.insert(out, '-')
  end
  
  -- TODO
  
  return table.concat(out)
end

]]
