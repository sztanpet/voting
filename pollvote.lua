local args            = ngx.req.get_uri_args()
local questionid      = tonumber( args.questionid )
local count           = tonumber( args.count ) -- if provided we shall wait for something to change
local maxsleepseconds = 10  -- the max time to wait for something to change
local sleepstep       = 0.1 -- how long to sleep for when nothing has changed
local sleeptotal      = 0

ngx.header.content_type = "application/json; charset=UTF-8"
if not questionid or questionid == 0 then
  ngx.say('{"success":false, "err":"invalidparams"}')
  return ngx.exit(ngx.HTTP_OK)
end

local redis       = (require "resty.redis"):new()
local questionkey = 'question_' .. questionid
local ret         = {success = true}

redis:set_timeout(1000)
redis:set_keepalive(0, 1000)
if not redis:connect("127.0.0.1", 6379) then
  ngx.log(ngx.ERR, "failed to connect to redis")
end

local function getData()
  
  local result = redis:hgetall( questionkey )
  
  if result[1] then
    
    local data      = {}
    local realcount = 0
    
    for i = 1, #result, 2 do
      data[ result[i] ] = result[i+1]
      realcount         = realcount + result[i+1]
    end
    
    return data, realcount
  else
    return nil, nil -- maybe it got deleted in the mean time?
  end
  
end

while sleeptotal < maxsleepseconds do
  local realcount
  ret.data, realcount = getData()
  -- if the count parameter is provided, wait for something to change
  if count and realcount and realcount == count then
    sleeptotal = sleeptotal + sleepstep
    ngx.sleep(sleepstep)
  elseif not ret.data then
    ngx.say('{"success":false, "err":"invalidparams"}')
    return ngx.exit(ngx.HTTP_OK)
  else
    break
  end
end

local json = require "cjson"
ngx.say(json.encode(ret))
