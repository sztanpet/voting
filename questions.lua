require('utility')
local db   = require('db')
local sess = require('session')

local function IDCheck(id, tbl)
  if not id or not tbl then redirect('/') end
  
  local sql = {
    "SELECT * FROM ", tbl, " WHERE id = '", id, "' LIMIT 1"
  }
  
  local result = db.query(sql)
  if not result then redirect('/') end
  
  return result[1]
end

local function userIDCheck(id, tbl, userid)
  if not id or not tbl then redirect('/') end
  if not userid then
    local session = sess.get()
    if not session or not session.user or not session.user.id then redirect('/') end
    userid = session.user.id
  end
  
  local sql = {
    "SELECT * FROM ", tbl,
    " WHERE id = '", id, "' AND userid = '", userid, "' LIMIT 1"
  }
  
  local result = db.query(sql)
  if not result then redirect('/') end
  
  return result[1]
end

local function getCreateQuestion()
  local session  = sess.get()
  
  if not session or not session.user then
    redirect('/')
  end
  
  local template = view('questions/question.tpl')
  local vars     = {member = session.user, create = true}
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local function postCreateQuestion()
  ngx.req.read_body()
  local success     = false
  local args        = ngx.req.get_post_args(10)
  local name        = trim(args.name)
  local description = trim(args.description)
  local ispublic    = tonumber(trim(args.ispublic)) == 1 or 0
  local session     = sess.get()
  local errormessage
  local questionid
  
  if not session or not session.user then
    redirect('/')
  end
  
  if #name == 0 then
    errormessage = 'You must specify a question!'
  end
  
  if not errormessage then
    name, description = db.escapePostgresParam(name, description)
    local sql = {
      "INSERT INTO questions (userid, name, description, ispublic) ",
      "VALUES ('", session.user.id, "', ", name, ", ", description, ", '", ispublic , "') RETURNING *"
    }
    
    local result = db.query(sql)
    if not result or not result[1] then
      ngx.log(ngx.ERR, "failed to db.query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    questionid = result[1].id
    success    = true
  end
  
  if success then
    redirect('/createanswer?questionid=' .. questionid)
  else
    local template = view('questions/question.tpl')
    local vars = {
      errormessage = errormessage,
      name         = name,
      description  = description,
      ispublic     = ispublic,
      create       = true,
      uri          = ngx.var.uri,
      member       = session.user,
    }
    
    sendHeaders()
    ngx.print(template(vars))
    return ngx.exit(ngx.HTTP_OK)
  end
end

local function getModifyQuestion()
  local args    = ngx.req.get_uri_args(10)
  local id      = tonumber(trim(args.id))
  local session = sess.get()
  
  if not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(id, "questions", session.user.id)
  if not result then
    redirect('/')
  end
  
  local template = view('questions/question.tpl')
  local vars     = {
    member      = session.user,
    uri         = ngx.var.uri .. '?id=' .. result.id,
    name        = result.name,
    description = result.description,
    ispublic    = result.ispublic,
  }
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local function postModifyQuestion()
  ngx.req.read_body()
  local success     = false
  local args        = ngx.req.get_post_args(10)
  local name        = trim(args.name)
  local description = trim(args.description)
  local ispublic    = tonumber(trim(args.ispublic)) == 1 and 1 or 0
  local session     = sess.get()
  args              = ngx.req.get_uri_args(10)
  local id          = tonumber(trim(args.id))
  local errormessage
  
  if not id or not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(id, "questions", session.user.id)
  if not result then
    redirect('/')
  end
  
  if #name == 0 then
    errormessage = 'You must specify a question!'
  end
  
  if not errormessage then
    name, description = db.escapePostgresParam(name, description)
    local sql = {
      "UPDATE questions SET (name, description, ispublic) = (",
      name, ", ", description, ", '", ispublic , "') ",
      "WHERE id = '", result.id , "' RETURNING *"
    }
    
    local result = db.query(sql)
    if not result or not result[1] then
      ngx.log(ngx.ERR, "failed to db.query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    success = true
  end
  
  if success then
    redirect('/questions')
  else
    local template = view('questions/question.tpl')
    local vars = {
      errormessage = errormessage,
      name         = name,
      description  = description,
      ispublic     = ispublic,
      uri          = ngx.var.uri,
      member       = session.user,
    }
    
    sendHeaders()
    ngx.print(template(vars))
    return ngx.exit(ngx.HTTP_OK)
  end
end

local function deleteQuestion()
  local args    = ngx.req.get_uri_args(10)
  local id      = tonumber(trim(args.id))
  local session = sess.get()
  
  if not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(id, "questions", session.user.id)
  if not result then
    redirect('/')
  end
  
  local sql    = "UPDATE questions SET disabled = 1 WHERE id = '" .. result.id .. "'"
  local result = db.query(sql)
  
  if not result then
    ngx.log(ngx.ERR, "failed to db.query postgres")
    return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
  end
  
  redirect('/questions')
end

local function getCreateAnswer()
  local session    = sess.get()
  local args       = ngx.req.get_uri_args(10)
  local questionid = tonumber(trim(args.questionid))
  
  if not questionid or not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(questionid, "questions", session.user.id)
  if not result then
    redirect('/')
  end
  
  local template = view('questions/answer.tpl')
  local vars     = {member = session.user, create = true}
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local function postCreateAnswer()
  ngx.req.read_body()
  local success     = false
  local args        = ngx.req.get_post_args(10)
  local name        = trim(args.name)
  local description = trim(args.description)
  local session     = sess.get()
  args              = ngx.req.get_uri_args(10)
  local questionid  = tonumber(trim(args.questionid))
  local errormessage
  
  if not questionid or not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(questionid, "questions", session.user.id)
  if not result then
    redirect('/')
  end
  
  local questionid = result.id
  
  if #name == 0 then
    errormessage = 'You must specify an answer!'
  end
  
  if not errormessage then
    name, description = db.escapePostgresParam(name, description)
    local sql = {
      "INSERT INTO answers (userid, questionid, name, description) ",
      "VALUES ('", session.user.id, "', '", questionid, "', ", name, ", ", description, ") RETURNING *"
    }
    
    local result = db.query(sql)
    if not result or not result[1] then
      ngx.log(ngx.ERR, "failed to db.query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    success = true
    
    result = db.query("SELECT id FROM answers WHERE disabled = 0 AND questionid = '" .. questionid .. "'")
    if not result then
      ngx.log(ngx.ERR, "failed to db.query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    local questionkey = 'question_' .. questionid
    local ipsetkey    = "ipset_question_" .. questionid
    local redis       = getRedisConnection()
    redis:init_pipeline()
    redis:del(questionkey)
    redis:del(ipsetkey)
    
    local hashfields = {
      questionkey,
    }
    
    for _, v in pairs(result) do
      table.insert(hashfields, v.id)
      table.insert(hashfields, 0)
    end
    
    redis:hmset(unpack(hashfields))
    redis:commit_pipeline()
  end
  
  if success then
    redirect('/createanswer?questionid=' .. questionid)
  else
    local template = view('questions/answer.tpl')
    local vars = {
      errormessage = errormessage,
      name         = name,
      description  = description,
      create       = true,
      uri          = ngx.var.uri,
      member       = session.user,
    }
    
    sendHeaders()
    ngx.print(template(vars))
    return ngx.exit(ngx.HTTP_OK)
  end
end

local function getModifyAnswer()
  local args    = ngx.req.get_uri_args(10)
  local id      = tonumber(trim(args.id))
  local session = sess.get()
  
  if not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(id, "answers", session.user.id)
  if not result then
    redirect('/')
  end
  
  local template = view('questions/answer.tpl')
  local vars     = {
    member      = session.user,
    uri         = ngx.var.uri .. '?id=' .. result.id,
    name        = result.name,
    description = result.description,
  }
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local function postModifyAnswer()
  ngx.req.read_body()
  local success     = false
  local args        = ngx.req.get_post_args(10)
  local name        = trim(args.name)
  local description = trim(args.description)
  local session     = sess.get()
  args              = ngx.req.get_uri_args(10)
  local id          = tonumber(trim(args.id))
  local errormessage
  
  if not id or not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(id, "answers", session.user.id)
  if not result then
    redirect('/')
  end
  
  if #name == 0 then
    errormessage = 'You must specify an answer!'
  end
  
  if not errormessage then
    name, description = db.escapePostgresParam(name, description)
    local sql = {
      "UPDATE answers SET (name, description) = (",
      name, ", ", description, ") WHERE id = '", result.id , "' RETURNING *"
    }
    
    local result = db.query(sql)
    if not result or not result[1] then
      ngx.log(ngx.ERR, "failed to db.query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    success = true
  end
  
  if success then
    redirect('/questions')
  else
    local template = view('questions/answer.tpl')
    local vars = {
      errormessage = errormessage,
      name         = name,
      description  = description,
      uri          = ngx.var.uri,
      member       = session.user,
    }
    
    sendHeaders()
    ngx.print(template(vars))
    return ngx.exit(ngx.HTTP_OK)
  end
end

local function deleteAnswer()
  local args    = ngx.req.get_uri_args(10)
  local id      = tonumber(trim(args.id))
  local session = sess.get()
  
  if not session or not session.user then
    redirect('/')
  end
  
  local result = userIDCheck(id, "answers", session.user.id)
  if not result then
    redirect('/')
  end
  
  result = db.query("UPDATE answers SET disabled = 1 WHERE id = '" .. result.id .. "' RETURNING *")
  
  if not result then
    ngx.log(ngx.ERR, "failed to db.query postgres")
    return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
  end
  
  local quesitonid  = result[1].questionid
  result = db.query("UPDATE answers SET count = 0 WHERE questionid = '" .. questionid .. "'")
  result = db.query("SELECT id FROM answers WHERE disabled = 0 AND questionid = '" .. questionid .. "'")
  local questionkey = 'question_' .. questionid
  local ipsetkey    = "ipset_question_" .. questionid
  local redis       = getRedisConnection()
  redis:init_pipeline()
  redis:del(questionkey)
  redis:del(ipsetkey)
  
  local hashfields = {
    questionkey,
  }
  
  for _, v in pairs(result) do
    table.insert(hashfields, v.id)
    table.insert(hashfields, 0)
  end
  
  redis:hmset(unpack(hashfields))
  redis:commit_pipeline()
  redirect('/questions')
end

local function questionDetails()
  local args       = ngx.req.get_uri_args(10)
  local id         = tonumber(trim(args.id))
  local session    = sess.get()
  local totalcount = 0
  
  local question = IDCheck(id, "questions")
  if not question then
    redirect('/')
  end
  
  local answers = db.query("SELECT * FROM answers WHERE disabled = '0' AND questionid = '" .. question.id .. "' ORDER BY RANDOM() LIMIT 100")
  if not answers then
    ngx.log(ngx.ERR, "failed to db.query postgres")
    return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
  end
  
  for k, v in pairs(answers) do
    totalcount = totalcount + v.count
  end
  
  for k, v in pairs(answers) do
    answers[k].percent = math.abs((v.count / totalcount) * 100)
  end
  
  local template = view('questions/details.tpl')
  local vars     = {
    member     = (session or {}).user,
    question   = question,
    answers    = answers,
    totalcount = totalcount,
  }
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local function questions()
  local session  = sess.get()
  
  if not session or not session.user then
    redirect('/')
  end
  
  local result = db.query(([[
    SELECT
      *,
      (
        SELECT SUM(count)
        FROM answers AS a
        WHERE
          a.questionid = q.id AND
          a.disabled   = 0
      ) AS count
    FROM questions AS q
    WHERE
      q.disabled = '0' AND
      q.userid   = '%s'
    LIMIT 100
  ]]):format(session.user.id))
  
  if not result then
    ngx.log(ngx.ERR, "failed to db.query postgres")
    return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
  end
  
  for k, v in pairs(result) do
    result[k]['answers'] = db.query("SELECT * FROM answers WHERE disabled = '0' AND questionid = '" .. v.id .. "' LIMIT 100")
    for ak, av in pairs(result[k]['answers']) do
      result[k]['answers'][ak]['percent'] = math.abs((av.count / v.count) * 100)
    end
  end
  
  local template = view('questions/questions.tpl')
  local vars     = {
    member    = session.user,
    questions = result,
  }
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local routes = {
  ['/q']              = questionDetails,
  ['/questions']      = questions,
  ['/createquestion'] = {get = getCreateQuestion, post = postCreateQuestion},
  ['/modifyquestion'] = {get = getModifyQuestion, post = postModifyQuestion},
  ['/deletequestion'] = deleteQuestion,
  ['/createanswer']   = {get = getCreateAnswer, post = postCreateAnswer},
  ['/modifyanswer']   = {get = getModifyAnswer, post = postModifyAnswer},
  ['/deleteanswer']   = deleteAnswer,
}

return function (uri)
  
  if type(routes[uri]) == 'table' then
    
    if ngx.var.request_method == 'POST' then
      routes[uri].post()
    else
      routes[uri].get()
    end
    
  else
    routes[uri]()
  end
end
