local args       = ngx.req.get_uri_args()
local questionid = tonumber( args.questionid )
local answerid   = tonumber( args.answerid )
ngx.header.content_type = "application/json; charset=UTF-8"

if not questionid or not answerid or questionid == 0 or answerid == 0 then
  ngx.say('{"success":false, "err":"invalidparams"}')
  return ngx.exit(ngx.HTTP_OK)
end

local ipaddress   = ngx.var.remote_addr
local redis       = (require "resty.redis"):new()
local questionkey = 'question_' .. questionid
local ipsetkey    = "ipset_question_" .. questionid
local results

local function commit_or_exit()
  local results, err = redis:commit_pipeline()

  if not results then
    ngx.log(ngx.ERR, "failed to query redis: " .. err )
  end
  
  return results
end

redis:set_timeout(1000)
redis:set_keepalive(0, 1000)
if not redis:connect("127.0.0.1", 6379) then
  ngx.log(ngx.ERR, "failed to connect to redis")
end

redis:init_pipeline()
redis:hexists( questionkey, answerid )
redis:sismember( ipsetkey, ipaddress )

results = commit_or_exit()

if results[1] ~= 1 then -- avoid setting cookie if the answer is non-existant
  ngx.say('{"success":false, "err":"invalidparams"}')
  return ngx.exit(ngx.HTTP_OK)
end

do
  local cookie      = {
    'vote-',
    questionid,
    '=',
    answerid,
    '; expires=',
    ngx.cookie_time( ngx.time() + 3240000 ),
    '; path=/;'
  }
  ngx.header['Set-Cookie'] = table.concat(cookie)
end

if results[2] == 1 then
  ngx.say('{"success":false, "err":"alreadyvoted"}')
  return ngx.exit(ngx.HTTP_OK)
end

redis:init_pipeline()

redis:sadd( ipsetkey, ipaddress ) -- there is a very small chance that the sadd will fail (race after ismember check, racing for sadd)
redis:hincrby( questionkey, answerid, 1 )
redis:hgetall( questionkey )

results = commit_or_exit()

if results[1] ~= 1 then -- failed to add ip, meaning the race really happened, handle it
  
  redis:init_pipeline()
  redis:hincrby( questionkey, answerid, -1 )
  redis:hgetall( questionkey )
  
  results = commit_or_exit()
  table.insert(results, 1, "") -- shift elements down, the code after us expects it so
  
end


local ret = {success = true, data = {}}

do
  
  local result = results[3]
  for i = 1, #result, 2 do
    ret.data[ result[i] ] = result[i+1]
  end
  
  local json = require "cjson"
  ngx.say(json.encode(ret))
  
end

-- try and sync the votes to postgres
local voting = ngx.shared.voting

if voting:get('postgresupdating') then
  return ngx.exit(ngx.HTTP_OK)
end

voting:set('postgresupdating', true)

ngx.flush()
local params = {method = ngx.HTTP_POST}

for k, v in pairs(ret.data) do
  
  params.body = "UPDATE answers SET count = '" .. v .. "' WHERE id = '" .. k .. "'"
  local result = ngx.location.capture("/postgres", params )
  if result.status ~= ngx.HTTP_OK or not result.body then
    error("failed to query postgres")
  end
  
end

voting:set('postgresupdating', false)
ngx.exit(ngx.HTTP_OK)
