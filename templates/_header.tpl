<!doctype html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="imagetoolbar" content="no" />
  <meta name="MSSmartTagsPreventParsing" content="true" />
  <meta name="format-detection" content="telephone=no" />
  <base href="http://voting.sztanpet.net/" /><!--[if IE]></base><![endif]-->
  <title>Voting</title>
  <link rel="StyleSheet" type="text/css" href="/static/css/style.css?mediaversion={{mediaversion}}" media="screen"/>

  <!--[if lte IE 8]>
  <link rel="StyleSheet" type="text/css" href="/static/css/style_ie.css?mediaversion={{mediaversion}}" />
  <![endif]-->

  <!--[if lte IE 6]>
  <link rel="StyleSheet" type="text/css" href="/static/css/style_ie6.css?mediaversion={{mediaversion}}" />
  <![endif]-->
  <script type="text/javascript" src="/static/js/jquery-1.7.2.min.js?mediaversion={{mediaversion}}"></script>
  <script type="text/javascript" src="/static/js/tools.js?mediaversion={{mediaversion}}"></script>
</head>
<body>
<div id="wrap">
  <div id="header">
    <a href="/"><span></span>Voting</a>
  </div>
  <div id="menu">
    {% if member then %}
      <div class="welcome ellipsize">Hi {<member.nickname>}.</div>
      <ul>
        <li><a href="/questions">My questions</a> |</li>
        <li><a href="/createquestion">Create question</a> |</li>
        <li><a href="/profile">Profile</a> |</li>
        <li><a href="/logout">Logout</a></li>
      </ul>
    {% else %}
      <div class="welcome ellipsize">Register now!</div>
      <ul>
        <li><a href="/login">Login</a> |</li>
        <li><a href="/register">Register</a></li>
      </ul>
    {% end %}
  </div>
  <div id="body">
