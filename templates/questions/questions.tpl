{("_header.tpl")}

<div class="title">
  <h1>My questions</h1>
</div>

<ul class="questionlist">
  {% for _, question in pairs(questions) do %}
    <li class="questionitem">
      <ul class="actions">
        <li class="new"><a href="/createanswer?questionid={<question.id>}">Create answer</a></li>
        <li class="modify"><a href="/modifyquestion?id={<question.id>}">Modify</a></li>
        <li class="delete"><a href="/deletequestion?id={<question.id>}" class="confirm" data-confirm="Are you sure?">Delete</a></li>
      </ul>
      <a href="/q?id={<question.id>}" class="question ellipsize">{<question.name>} ({<question.count>})</a>
      
      <ul class="answerlist">
        {% for _, answer in pairs(question.answers) do %}
          <li class="answeritem">
            <ul class="actions">
              <li class="modify"><a href="/modifyanswer?id={<answer.id>}">Modify</a></li>
              <li class="delete"><a href="/deleteanswer?id={<answer.id>}" class="confirm" data-confirm="Deleting an answer will reset the counters for the whole question. Are you sure?">Delete</a></li>
            </ul>
            <div class="answer ellipsize">{<answer.name>} ({<answer.count>})</div>
            <div class="answerpercent"><div class="bar" style="width: {<answer.percent>}%"></div></div>
          </li>
        {% end %}
      </ul>
    </li>
  {% end %}
</ul>

{("_footer.tpl")}