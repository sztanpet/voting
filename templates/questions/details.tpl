{("_header.tpl")}
<script type="text/javascript">
  var pollurl = '/pollvote?questionid={{question.id}}&count={{totalcount}}'
</script>
<div class="title" id="votingpage">
  <h1>{<question.name>} (<span class="count">{{totalcount}}</span>)</h1>
</div>

{% if question.description ~= ngx.null and #question.description > 0 then %}
  <p class="description">
    {{nl2br(escape(question.description))}}
  </p>
{% end %}

<ul class="answerlist" data-totalcount="{<totalcount>}" data-questionid="{{question.id}}">
  {% for _, answer in pairs(answers) do %}
    <li class="answeritem" id="answer-{<answer.id>}" data-answerid="{{answer.id}}">
      {% if member and answer.userid == member.id then %}
        <ul class="actions">
          <li class="modify"><a href="/modifyanswer?id={<answer.id>}">Modify</a></li>
          <li class="delete"><a href="/deleteanswer?id={<answer.id>}" class="confirm" data-confirm="Deleting an answer will reset the counters for the whole question. Are you sure?">Delete</a></li>
        </ul>
      {% end %}
      <a href="/vote?questionid={<question.id>}&amp;answerid={<answer.id>}" class="answer">{<answer.name>} (<span class="count">{<answer.count>}</span>)</a>
      <div class="answerpercent"><div class="bar" style="width: {<answer.percent>}%"></div></div>
    </li>
  {% end %}
</ul>

{("_footer.tpl")}