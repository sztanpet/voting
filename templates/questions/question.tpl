{("_header.tpl")}

<div class="title">
  {% if create then %}
    <h1>Create question</h1>
  {% else %}
    <h1>Modify question</h1>
  {% end %}
</div>

<p class="info">
  A non-public question is meant for the time when there are non answers yet for the question.<br/>
  Make the question public after creating your answers
</p>

{% if errormessage then %}
  <div class="formerrors">
    {<errormessage>}
  </div>
{% end %}

<form action="{<uri>}" method="POST" enctype="application/x-www-form-urlencoded" id="question" name="question">
  <table>
    <tr>
      <td class="labelcolumn"><label for="name">Question</label></td>
      <td class="elementcolumn"><input type="text" name="name" id="name" value="{<name>}"/></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="description">Description</label></td>
      <td class="elementcolumn"><textarea name="description" id="description"/>{% if description ~= ngx.null then %}{<description>}{% end %}</textarea></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="ispublic">Public?</label></td>
      <td class="elementcolumn"><input type="checkbox" value="1" name="ispublic" id="ispublic"{% if ispublic == 1 then %} checked="checked"{% end %}/></td>
    </tr>
    <tr class="submitrow">
      <td colspan="2"><input type="submit" value="Go"/></td>
    </tr>
  </table>
</form>

{("_footer.tpl")}