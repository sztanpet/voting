{("_header.tpl")}

<div class="title">
  {% if create then %}
    <h1>Create answer</h1>
  {% else %}
    <h1>Modify answer</h1>
  {% end %}
</div>

{% if errormessage then %}
  <div class="formerrors">
    {<errormessage>}
  </div>
{% end %}

<form action="{<uri>}" method="POST" enctype="application/x-www-form-urlencoded" id="answer" name="answer">
  <table>
    <tr>
      <td class="labelcolumn"><label for="name">Answer</label></td>
      <td class="elementcolumn"><input type="text" name="name" id="name" value="{<name>}"/></td>
    </tr>
    <tr class="submitrow">
      <td colspan="2"><input type="submit" value="{% if create then %}Go and create a new one{% else %}Go{% end %}"/></td>
    </tr>
  </table>
</form>

{("_footer.tpl")}