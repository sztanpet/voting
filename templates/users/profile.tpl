{("_header.tpl")}

<div class="title">
  <h1>Profile</h1>
</div>

<p class="info">
  If you do not want to change your password, leave it blank!
</p>

{% if errormessage then %}
  <div class="formerrors">
    {<errormessage>}
  </div>
{% end %}

<form action="/profile" method="POST" enctype="application/x-www-form-urlencoded" id="profile" name="profile">
  <table>
    <tr>
      <td class="labelcolumn"><label for="nickname">Nickname</label></td>
      <td class="elementcolumn"><input type="text" name="nickname" id="nickname" value="{<nickname>}"/></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="password">Password</label></td>
      <td class="elementcolumn"><input type="password" name="password" id="password" value="{<password>}"/></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="confirmpassword">Confirm password</label></td>
      <td class="elementcolumn"><input type="password" name="confirmpassword" id="confirmpassword" value="{<confirmpassword>}"/></td>
    </tr>
    <tr class="submitrow">
      <td colspan="2"><input type="submit" value="Go"/></td>
    </tr>
  </table>
</form>

{("_footer.tpl")}