{("_header.tpl")}

<div class="title">
  <h1>Login</h1>
</div>

{% if errormessage then %}
  <div class="formerrors">
    {<errormessage>}
  </div>
{% end %}

<form action="/login" method="POST" enctype="application/x-www-form-urlencoded" id="login" name="login">
  <table>
    <tr>
      <td class="labelcolumn"><label for="email">Email</label></td>
      <td class="elementcolumn"><input type="text" name="email" id="email" value="{<email>}"/></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="password">Password</label></td>
      <td class="elementcolumn"><input type="password" name="password" id="password" value="{<password>}"/></td>
    </tr>
    <tr class="submitrow">
      <td colspan="2"><input type="submit" value="Go"/></td>
    </tr>
  </table>
</form>

{("_footer.tpl")}