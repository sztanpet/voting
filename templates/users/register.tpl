{("_header.tpl")}

<div class="title">
  <h1>Register</h1>
</div>

<p class="info">
  We will not use your e-mail address for anything other than authentication<br/>
  It is not validated, nor used to send you any kind of e-mail.<br/>
  Every field is mandatory.
</p>

{% if errormessage then %}
  <div class="formerrors">
    {<errormessage>}
  </div>
{% end %}

<form action="/register" method="POST" enctype="application/x-www-form-urlencoded" id="register" name="register">
  <table>
    <tr>
      <td class="labelcolumn"><label for="email">Email</label></td>
      <td class="elementcolumn"><input type="text" name="email" id="email" value="{<email>}"/></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="nickname">Nickname</label></td>
      <td class="elementcolumn"><input type="text" name="nickname" id="nickname" value="{<nickname>}"/></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="password">Password</label></td>
      <td class="elementcolumn"><input type="password" name="password" id="password" value="{<password>}"/></td>
    </tr>
    <tr>
      <td class="labelcolumn"><label for="confirmpassword">Confirm password</label></td>
      <td class="elementcolumn"><input type="password" name="confirmpassword" id="confirmpassword" value="{<confirmpassword>}"/></td>
    </tr>
    <tr class="submitrow">
      <td colspan="2"><input type="submit" value="Go"/></td>
    </tr>
  </table>
</form>

{("_footer.tpl")}