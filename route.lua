--[==[
  http://wiki.nginx.org/HttpGzipModule
  
  # set search paths for pure Lua external libraries (';;' is the default path):
  nginx config: lua_package_path '/foo/bar/?.lua;/blah/?.lua;;';
  
  ngx.req.set_header("Content-Type", "text/css")
  ngx.header.content_type = "application/json; charset=UTF-8"
  Content-type: text/html; charset=utf-8
  X-UA-Compatible: chrome=1
  
  ngx.req.read_body()
  ngx.req.get_post_args(10) -- application/x-www-form-urlencoded
  
  ngx.var.request_method -- hogy kulonbseget lehessen tenni get/post kozott
  
  nem kell parsolni cookiet, latszolag megteszi az nginx
  ngx.var['cookie_VOTE-1']
  
  sessionid-hez:
  https://github.com/agentzh/set-misc-nginx-module#set_secure_random_alphanum
  set_secure_random_alphanum $sessid 32;
  
  session_regenerate_id csak felulirja a sessionid cookie-t es atnevezi a sessiont
  lekerdezzuk a sessiont
  GET votingsession:32bytesessionid
  -- aztan regeneratenel ket lehetoseg
  DEL votingsession:32bytesessionid
  SETEX votingsession:new32bytesessionid seconds serializedsessiondata
  --
  SETNX --et hasznalhatunk de akkor kulon
  EXPIRE key seconds -- hivas is kell ha kezelni akarjuk a duplikalt sessionidket
  ha nullat kapunk vissza akkor ujra generalni a sessionid-t mert duplikalt, wtf
  -- vagy
  RENAME/RENAMENX -- ha sikerul, csak akkor settelni a cookie headert esetleg?
  -- hash is ertelmes lenne de a kulcsokra kulon nem lehet expireolni
  
  login
  register
  logout
  createquestion
  createvote
  modifyquestion
  modifyvote
  deletevote
  deletequestion
  
]==]

local uri  = ngx.var.uri
local urls = {
  ['/']               = 'index',
  ['/index']          = 'index',
  
  ['/login']          = 'users',
  ['/logout']         = 'users',
  ['/register']       = 'users',
  ['/profile']        = 'users',
  
  ['/questions']      = 'questions',
  ['/q']              = 'questions',
  ['/createquestion'] = 'questions',
  ['/modifyquestion'] = 'questions',
  ['/deletequestion'] = 'questions',
  ['/createanswer']   = 'questions',
  ['/modifyanswer']   = 'questions',
  ['/deleteanswer']   = 'questions',
  ['/createvote']     = 'questions',
  ['/modifyvote']     = 'questions',
  ['/deletevote']     = 'questions',
}

if urls[ uri ] then
  (require(urls[ uri ]))(uri)
else
  ngx.exit(ngx.HTTP_NOT_FOUND)
end
