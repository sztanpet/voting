require('utility')
local sess = require('session')
local db   = require('db')

local function getLogin()
  local template = view('users/login.tpl')
  local vars     = {}
  local session  = sess.get()
  
  if session and session.user then
    vars.member = session.user
  end
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local function getPasswordHash(password)
  local digest = ngx.hmac_sha1(ngx.var.salt, password)
  
  return ngx.encode_base64(digest)
end

-- very dumb email validation
local function validEmail(email)
  if not email then return end
  
  return (email:match("[A-Za-z0-9%.%%%+%-]+@[A-Za-z0-9%.%%%+%-]+%.%w%w%w?%w?"))
end

local function postLogin()
  ngx.req.read_body()
  local success  = false
  local args     = ngx.req.get_post_args(10)
  local email    = validEmail(trim(args.email))
  local password = trim(args.password)
  local vars     = {
    email    = trim(args.email),
    password = password,
  }
  
  if email and #email > 0 and #password > 0 then
    email, password = db.escapePostgresParam(email, getPasswordHash(password))
    
    local result = db.query("SELECT * FROM users WHERE email = " .. email .. " AND password = " .. password .. " LIMIT 1")
    
    if not result then
      ngx.log(ngx.ERR, "failed to query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    local data = {['user'] = result[1]}
    
    if data.user and next(data.user) then
      success = true
      sess.start(data)
    end
  end
  
  if success then
    redirect('/')
  else
    local template    = view('users/login.tpl')
    if not email then
      vars.errormessage = 'Invalid email!'
    else
      vars.errormessage = 'Access denied!'
    end
    
    sendHeaders()
    ngx.print(template(vars))
    return ngx.exit(ngx.HTTP_OK)
  end
end

local function logout()
  sess.destroy()
  redirect('/')
end

local function getRegister()
  local template = view('users/register.tpl')
  local vars     = {}
  local session  = sess.get()
  
  if session and session.user then
    vars.member = session.user
  end
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

local function postRegister()
  ngx.req.read_body()
  local success         = false
  local args            = ngx.req.get_post_args(10)
  local email           = validEmail(trim(args.email))
  local nickname        = trim(args.nickname)
  local password        = trim(args.password)
  local confirmpassword = trim(args.confirmpassword)
  local errormessage
  local vars = {
    email           = trim(args.email),
    nickname        = nickname,
    password        = password,
    confirmpassword = confirmpassword,
  }
  
  if not email or #email == 0 or #nickname == 0 or #password == 0 or #confirmpassword == 0 then
    if not email then
      errormessage = 'Invalid e-mail address! Example: username@domain.tld'
    else
      errormessage = 'Every field is mandatory!'
    end
  end
  
  if password ~= confirmpassword then
    errormessage = 'The two passwords do not match!'
  end
  
  if not errormessage then
    email, nickname, password = db.escapePostgresParam(email, nickname, getPasswordHash(password))
    
    local result = db.query('SELECT COUNT(*) AS count FROM users WHERE email = ' .. email)
    
    if not result then
      ngx.log(ngx.ERR, "failed to query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    if not result[1] or result[1].count ~= 0 then
      errormessage = 'A user with that e-mail address is already registered!'
    end
  end
  
  if not errormessage then
    
    local result = db.query('SELECT COUNT(*) AS count FROM users WHERE nickname = ' .. nickname)
    
    if not result then
      ngx.log(ngx.ERR, "failed to query postgres")
      return ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    end
    
    if not result[1] or result[1].count ~= 0 then
      errormessage = 'A user with that nickname is already registered!'
    end
  end
  
  if not errormessage then
    
    local sql = {
      "INSERT INTO users (email, nickname, password) ",
      "VALUES (", email, ", ", nickname, ", ", password, ") RETURNING *"
    }
    
    local result = db.query(sql)
    
    if not result then
      errormessage = 'A user with that nickname/email already exists, please choose another one.'
    end
    
    if not errormessage then
      local data = {['user'] = result[1]}
      
      if data.user and next(data.user) then
        success = true
        sess.start(data)
      end
    end
  end
  
  if success then
    redirect('/')
  else
    local template    = view('users/register.tpl')
    vars.errormessage = errormessage
    
    sendHeaders()
    ngx.print(template(vars))
    return ngx.exit(ngx.HTTP_OK)
  end
end

function getProfile()
  local template = view('users/profile.tpl')
  local vars     = {}
  local session  = sess.get()
  
  if not session or not session.user then
    redirect('/')
  end
  
  vars.member   = session.user
  vars.nickname = session.user.nickname
  
  sendHeaders()
  ngx.print(template(vars))
  return ngx.exit(ngx.HTTP_OK)
end

function postProfile()
  ngx.req.read_body()
  local success         = false
  local args            = ngx.req.get_post_args(10)
  local nickname        = trim(args.nickname)
  local password        = trim(args.password)
  local confirmpassword = trim(args.confirmpassword)
  local session         = sess.get()
  local vars            = {
    nickname = nickname,
  }
  local errormessage
  
  if not session or not session.user then
    redirect('/')
  end
  
  vars.member = session.user
  
  if #nickname == 0 then
    errormessage = 'The nickname is mandatory!'
  end
  
  if ( #password > 0 or #confirmpassword > 0 ) and password ~= confirmpassword then
    errormessage = 'The two passwords do not match!'
  elseif #password == 0 and #confirmpassword == 0 then
    password, confirmpassword = nil, nil
  end
  
  if not errormessage then
    local fields
    local sql
    
    if password then
      nickname, password = db.escapePostgresParam(nickname, getPasswordHash(password))
      fields = {
        nickname = nickname,
        password = password,
      }
      
      sql = {
        'UPDATE users SET nickname = ', nickname,
        ', password = ', password,
        " WHERE id = '", session.user.id , "' RETURNING *"
      }
      
    else
      nickname = db.escapePostgresParam(nickname)
      fields   = {nickname = nickname}
      sql      = {
        'UPDATE users SET nickname = ', nickname,
        " WHERE id = '", session.user.id , "' RETURNING *"
      }
      
    end
    
    local result = db.query(sql)
    if not result then
      errormessage = 'A user with that nickname already exists, please choose a different one.'
    end
    
    if not errormessage then
      session.user =  result[1]
      
      success = true
      sess.sync(nil, session)
    end
  end
  
  if success then
    redirect('/')
  else
    local template    = view('users/profile.tpl')
    vars.errormessage = errormessage
    
    sendHeaders()
    ngx.print(template(vars))
    return ngx.exit(ngx.HTTP_OK)
  end
end

local routes = {
  ['/login']    = {get = getLogin, post = postLogin},
  ['/register'] = {get = getRegister, post = postRegister},
  ['/profile']  = {get = getProfile, post = postProfile},
  ['/logout']   = logout,
}

return function (uri)
  
  if type(routes[uri]) == 'table' then
    
    if ngx.var.request_method == 'POST' then
      routes[uri].post()
    else
      routes[uri].get()
    end
    
  else
    routes[uri]()
  end
end
