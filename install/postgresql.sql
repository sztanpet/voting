--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: vote; Type: SCHEMA; Schema: -; Owner: sztanpet
--

CREATE SCHEMA vote;

SET search_path = vote, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: answers; Type: TABLE; Schema: vote; Owner: sztanpet; Tablespace: 
--

CREATE TABLE answers (
    id bigint NOT NULL,
    questionid bigint NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    count bigint DEFAULT 0 NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    disabled smallint DEFAULT 0 NOT NULL,
    userid bigint NOT NULL
);

--
-- Name: answers_id_seq; Type: SEQUENCE; Schema: vote; Owner: sztanpet
--

CREATE SEQUENCE answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: answers_id_seq; Type: SEQUENCE OWNED BY; Schema: vote; Owner: sztanpet
--

ALTER SEQUENCE answers_id_seq OWNED BY answers.id;

--
-- Name: questions; Type: TABLE; Schema: vote; Owner: sztanpet; Tablespace: 
--

CREATE TABLE questions (
    id bigint NOT NULL,
    userid bigint NOT NULL,
    name character varying(255) NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    description text,
    ispublic smallint DEFAULT 0 NOT NULL,
    disabled smallint DEFAULT 0 NOT NULL
);

--
-- Name: questions_id_seq; Type: SEQUENCE; Schema: vote; Owner: sztanpet
--

CREATE SEQUENCE questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: questions_id_seq; Type: SEQUENCE OWNED BY; Schema: vote; Owner: sztanpet
--

ALTER SEQUENCE questions_id_seq OWNED BY questions.id;

--
-- Name: users; Type: TABLE; Schema: vote; Owner: sztanpet; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    nickname character varying(30) NOT NULL,
    password character varying(40) NOT NULL,
    email character varying(50) NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    isadmin smallint DEFAULT 0 NOT NULL,
    disabled smallint DEFAULT 0 NOT NULL
);

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: vote; Owner: sztanpet
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: id; Type: DEFAULT; Schema: vote; Owner: sztanpet
--

ALTER TABLE ONLY answers ALTER COLUMN id SET DEFAULT nextval('answers_id_seq'::regclass);

--
-- Name: id; Type: DEFAULT; Schema: vote; Owner: sztanpet
--

ALTER TABLE ONLY questions ALTER COLUMN id SET DEFAULT nextval('questions_id_seq'::regclass);

--
-- Name: id; Type: DEFAULT; Schema: vote; Owner: sztanpet
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: answers_pkey; Type: CONSTRAINT; Schema: vote; Owner: sztanpet; Tablespace: 
--

ALTER TABLE ONLY answers
    ADD CONSTRAINT answers_pkey PRIMARY KEY (id);


--
-- Name: questions_pkey; Type: CONSTRAINT; Schema: vote; Owner: sztanpet; Tablespace: 
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);

--
-- Name: users_email_key; Type: CONSTRAINT; Schema: vote; Owner: sztanpet; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users_nickname; Type: CONSTRAINT; Schema: vote; Owner: sztanpet; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_nickname UNIQUE (nickname);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: vote; Owner: sztanpet; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

--
-- Name: question; Type: INDEX; Schema: vote; Owner: sztanpet; Tablespace: 
--

CREATE INDEX question ON answers USING btree (questionid);


--
-- Name: userid; Type: INDEX; Schema: vote; Owner: sztanpet; Tablespace: 
--

CREATE INDEX userid ON questions USING btree (userid);

--
-- PostgreSQL database dump complete
--
